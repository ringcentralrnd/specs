Pod::Spec.new do |s|
  s.name = 'Talk-SDK'
  s.version = '0.2'
  s.license = { :type => 'kind of BSD', :text => 'See http://www.webrtc.org/license-rights/license' }
  s.summary = 'iOS framework for WebRTC'
  s.description = <<-DESC
                    iOS framework for WebRTC with libjingle part
                     DESC
  s.homepage = 'http://www.webrtc.org'
  s.author = { 'Roman Gayu' =>  'roman.gayu@gmail.com' }

  s.source = { :http => 'https://bitbucket.org/ringcentralrnd/webrtcbuild/downloads/Talk-SDK-0.2.tar.gz', :type => :tgz }

  s.ios.vendored_frameworks = "talk.framework"

  s.ios.weak_framework = "WebRTC-SDK"

  s.ios.deployment_target = '7.0'
end