Pod::Spec.new do |s|
  s.name = 'WebRTC-SDK'
  s.version = '0.4'
  s.license = { :type => 'kind of BSD', :text => 'See http://www.webrtc.org/license-rights/license' }
  s.summary = 'iOS framework for WebRTC'
  s.description = <<-DESC
                      WebRTC is a free, open project that enables web browsers with Real-Time Communications (RTC) capabilities via simple JavaScript APIs. The WebRTC components have been optimized to best serve this purpose.   
                     DESC
  s.homepage = 'http://www.webrtc.org'
  s.author       = 'www.webrtc.org'
  s.source = { :http => 'http://bitbucket.org/ringcentralrnd/webrtcbuild/downloads/WebRTC-SDK-0.4.tar.gz', :type => :tgz }

  s.ios.vendored_frameworks = "webrtc.framework"
  s.frameworks = 'CoreMedia', 'AVFoundation'    
  s.ios.deployment_target = '6.0'
#  s.library = 'c++'
end