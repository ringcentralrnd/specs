Pod::Spec.new do |s|
  s.name = 'WebRTC-SDK'
  s.version = '0.1'
  s.license = { :type => 'kind of BSD', :text => 'See http://www.webrtc.org/license-rights/license' }
  s.summary = 'iOS framework for WebRTC'
  s.description = <<-DESC
                      SHARK provides libraries for the design of adaptive systems, including methods for linear and nonlinear optimization (e.g., evolutionary and gradient-based algorithms), kernel-based algorithms and neural networks, and other machine learning techniques.
                     DESC
  s.homepage = 'http://www.webrtc.org'
  s.author = { 'Roman Gayu' =>  'roman.gayu@gmail.com' }

  s.source = { :http => 'https://bitbucket.org/hamst/webrtcbuild/downloads/WebRTC-SDK.tar.gz', :type => :tgz }

  s.ios.vendored_frameworks = "webrtc.framework"

  s.ios.deployment_target = '7.0'
end