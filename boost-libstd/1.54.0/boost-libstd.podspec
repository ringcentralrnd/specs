Pod::Spec.new do |s|
  s.name = 'boost-libstd'
  s.version = '1.54.0'
  s.summary = "Boost provides free peer-reviewed portable C++ source libraries."
  s.homepage  = "http://www.boost.org"
  s.license = { :type => "Boost Software License", :text => 'See http://www.webrtc.org/license-rights/license' }
  s.author       = 'www.boost.org'
  s.source = { :http => 'https://bitbucket.org/ringcentralrnd/boostbuild/downloads/boost154.framework.zip' }

  s.ios.vendored_frameworks = "boost.framework"
  s.ios.deployment_target = '6.0'
end